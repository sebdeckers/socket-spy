# socket-spy

Handly logger for plaintext socket traffic.

## Use Cases

Originally developed to inspect HTTP `multipart/form-data` request bodies with nested file paths in [`@http/api`](https://gitlab.com/http2/api) for the [http2.live](https://http2.live/) service.

Also useful for:

- Inspecting a single, low-traffic connection in development.
- Viewing the raw network data to debug problems.
- Understanding low-level protocol details.
- Satisfying a healthy sense of technical curiosity.

Definitely not useful for:

- Production
- Performance
- Security

## Usage

Print all incoming HTTP requests in the terminal as raw data.

```js
const server = require('http').createServer()
server.on('connection', require('socket-spy'))
server.listen(0, () => {
  console.log(`Listening on port ${server.address().port}`)
})
```
