function onData (data) {
  process.stdout.write(data)
}

function onEnd () {
  this.removeListener('data', onData)
}

function onConnection (socket) {
  socket.on('data', onData)
  socket.once('end', onEnd)
}

module.exports = onConnection
